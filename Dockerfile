FROM alpine:3.19.0

LABEL maintainer="Damiano Verzulli <damiano@verzulli.it>"

RUN apk add --no-cache openvpn bash

ADD run.sh /

WORKDIR /

CMD [ "/bin/bash", "run.sh" ]
