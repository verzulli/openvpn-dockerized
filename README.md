DV OpenVpn docker image
=======================

What's this?
------------

I needed a **BASIC** OpenVPN docker image, to be given my specific */etc/openvpn* folder 
so to start a **SIMPLE** OpenVPN server. That's it. No other frills (...as all the certificate
issues are normally handled by me, on my local system)

How can I use it?
-----------------
Just fire a container with this image...but remember to
*mount* a volume in */etc/openvpn* that will contain:

* a *server.ovpn* file with a working configuration
* any other required files (like, for example, the *ccd* folder)

In short: if you have a "working" /etc/openvpn folder with
an included *server.ovpn* file, than this container is for you