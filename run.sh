/bin/bash

if [[ ! -d "/etc/openvpn" ]]; then
   echo "Missing /etc/openvpn folder. It's required! Cannot continue!"
   sleep 600
   exit
fi
if [[ ! -e "/etc/openvpn/server.ovpn" ]]; then
   echo "Missing /etc/openvpn/server.ovpn file. It's required! Cannot continue!"
   sleep 600
   exit
fi
echo "=> Current /etc/openvpn dir:"
ls -l /etc/openvpn
echo "----------------------------"
echo
echo "=> Current /etc/openvpn/server.ovpn"
echo "------------------------"
cat /etc/openvpn/server.ovpn
echo "------------------------"
echo ""

mkdir -p /dev/net
if [ ! -c /dev/net/tun ]; then
    mknod /dev/net/tun c 10 200
fi

echo "=> Running: [cd /etc/openvpn ; openvpn ./server.ovpn]"
cd /etc/openvpn
openvpn ./server.ovpn
